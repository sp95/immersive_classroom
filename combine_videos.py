import cv2
import numpy as np
import sys

cap1 = cv2.VideoCapture(0)
cap2 = cv2.VideoCapture('http://192.168.42.129:8080/video')
decision = sys.argv[1]

while(cap1.isOpened()):

    ret1, frame1 = cap1.read()
    new_frame1 = cv2.resize(frame1,(650,700))
    ret2, frame2 = cap2.read()
    new_frame2 = cv2.resize(frame2,(650,700))
    if ret1 == True and ret2 == True:
        both = np.concatenate((new_frame1, new_frame2), axis=1)
        #both = cv2.hconcat([new_frame, new_frame2])
        if(decision=="1"):
            cv2.imshow('frame1', new_frame1)
        elif(decision=="2"):
            cv2.imshow('frame2', new_frame2)
        elif(decision=="360"):
            cv2.imshow('both', both)
        else:
            pass
         
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    else: 
        break

cap1.release()
cap2.release()
cv2.waitKey(0)
cv2.destroyAllWindows()
